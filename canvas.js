let canvas = document.getElementById('mycanvas');
let ctx = canvas.getContext('2d');
let strokeColor = 'black';
let fillColor = 'black';
let line_Width = 2;
let currentTool = 'Brush';
let isDrawing = false;
let x = 0;
let y = 0;
let size = 8;
let Pen_Color = 'black';
var font = 'Arial';
let state = ctx.getImageData(0, 0, canvas.width, canvas.height);
window.history.pushState(state, null);
let font_style = "px Georgia";
let brush = false;
let eraser = false;
let t = false;
let circle = false;
let triangle = false;
let rectangle = false;
var isPress = false;
var old = null;

canvas.addEventListener('click', function(e){
  if(brush == true){

  }else if(circle == true){
    var range = size * 5;
      ctx.beginPath();
      ctx.arc(e.clientX-50, e.clientY-350, range, 0, 2*Math.PI);
      console.log(e.clientX, e.clientY);
      ctx.closePath();
      ctx.fill();
      ctx.strokeStyle = Pen_Color;
      ctx.fillStyle = Pen_Color;
      ctx.stroke();
      var TMP = ctx.getImageData(0, 0, canvas.width, canvas.height);
      window.history.pushState(TMP, null);
  }else if(triangle == true){
     var range = size * 5;
        ctx.beginPath();
        ctx.moveTo(e.clientX-500, e.clientY-150-range-150-100);
        ctx.lineTo(e.clientX+range-500, e.clientY-290-100);
        ctx.lineTo(e.clientX-range-500, e.clientY-290-100);
        ctx.closePath();
        ctx.strokeStyle = Pen_Color;
        ctx.fillStyle = Pen_Color;
        ctx.fill();
        ctx.stroke();   
        var TMP = ctx.getImageData(0, 0, canvas.width, canvas.height);
        window.history.pushState(TMP, null); 
  }else if(rectangle == true){
      var range = size * 5;
        ctx.beginPath();
        ctx.moveTo(e.clientX-450, e.clientY-400);
        ctx.lineTo(e.clientX+range/2-450, e.clientY-400);
        ctx.lineTo(e.clientX+range/2-450, e.clientY-range-400);
        ctx.lineTo(e.clientX-range*1.5-450, e.clientY-range-400);
        ctx.lineTo(e.clientX-range*1.5-450, e.clientY-400);
        ctx.lineTo(e.clientX-range-450, e.clientY-400);
        ctx.closePath();
        ctx.strokeStyle = Pen_Color;
        ctx.fillStyle = Pen_Color;
        ctx.fill();
        ctx.stroke();
        var TMP = ctx.getImageData(0, 0, canvas.width, canvas.height);
        window.history.pushState(TMP, null);
  }
  console.log(rectangle);
  console.log(triangle);
})



function ChangeTool(toolClicked){
  
    if(toolClicked === 'brush'){
      brush = true;
      eraser = false;
      t = false;
      circle = false;
      triangle = false;
      rectangle = false;
      document.getElementById("mycanvas").className = "brushpng";
      
    }
    else if(toolClicked === 'eraser'){
      brush = false;
      eraser = true;
      t = false;
      circle = false;
      triangle = false;
      rectangle = false;
      document.getElementById("mycanvas").className = "eraserpng";
    }
    else if(toolClicked === 't'){
      brush = false;
      eraser = false;
      t = true;
      circle = false;
      triangle = false;
      rectangle = false;
      document.getElementById("mycanvas").className = "typeepng";
    }
    else if(toolClicked === 'circle'){
      brush = false;
      eraser = false;
      t = false;
      circle = true;
      triangle = false;
      rectangle = false;
      document.getElementById("mycanvas").className = "circlepng";
      
    }
    else if(toolClicked === 'triangle'){
      brush = false;
      eraser = false;
      t = false;
      circle = false;
      triangle = true;
      rectangle = false;
      document.getElementById("mycanvas").className = "trianglepng";
      }
    
    else if(toolClicked === 'rectangle'){
      console.log('hi');
      brush = false;
      eraser = false;
      t = false;
      circle = false;
      triangle = false;
      rectangle = true;
      document.getElementById("mycanvas").className = "rectanglepng";
      }
    }
  


function changeStep(e){
  
  console.log(eraser);
  if( e.state ){
    
    console.log('in');
    ctx.putImageData(e.state, 0, 0);
  }
}

window.addEventListener('popstate', changeStep, false); //undo redo
const rect = canvas.getBoundingClientRect();

canvas.addEventListener('mousedown', e => {
  x = e.clientX - rect.left;
  y = e.clientY - rect.top;
  isDrawing = true;
});

canvas.addEventListener('mousemove', e => {
  if (isDrawing === true && brush===true) {
    drawLine(ctx, x, y, e.clientX - rect.left, e.clientY - rect.top);
    console.log(ctx.strokeColor);
    console.log(ctx.fillColor);
    x = e.clientX - rect.left;
    y = e.clientY - rect.top;
  }
});

window.addEventListener('mouseup', e => {
  if (isDrawing === true && brush===true) {
    drawLine(ctx, x, y, e.clientX - rect.left, e.clientY - rect.top);
    x = 0;
    y = 0;
    isDrawing = false;
    var state = ctx.getImageData(0, 0, canvas.width, canvas.height);
    window.history.pushState(state, null);
  }
});




function drawLine(ctx, x1, y1, x2, y2) {
  ctx.beginPath();
  ctx.strokeStyle = Pen_Color;
  ctx.fillColor = Pen_Color;
  ctx.lineWidth = size;
  ctx.moveTo(x1, y1);
  ctx.lineTo(x2, y2);
  ctx.stroke();
  ctx.closePath();
}
 
function changecolor(color){
   console.log(color);
   Pen_Color = color;
}

function changefont(font){
  font_style= "px " + font;
}

var mouseX = 0;
var mouseY = 0;
var startingX = 0;



  canvas.addEventListener("click", function(e){
    mouseX = e.pageX - canvas.offsetLeft;
    mouseY = e.pageY - canvas.offsetTop;
    startingX = mouseX;
    return false;
  }, false);

  document.addEventListener("keyup", function(e){
    if(t===true){
      if(e.keyCode ===13){
        mouseX = startingX;
        mouseY += 20;
      }else{
        var test_font = size.toString() + font_style;
        console.log(test_font);
        ctx.font = test_font;
        ctx.fillText(e.key, mouseX, mouseY);
        ctx.fillStyle = Pen_Color;
        mouseX += ctx.measureText(e.key).width;
      }
    }
    
  }, false);


function small() { 
  size = 8;
}

function med() {
  size = 12;
}

function big() {
  size = 24;
}

function download() { 
  var save = document.getElementById("download");
  var image = document.getElementById("mycanvas").toDataURL("image/png")
      .replace("image/png", "image/octet-stream");
  save.setAttribute("href", image);
}
 
function Refresh(){
  ctx.clearRect(0, 0, canvas.width, canvas.height);
}

document.getElementById('upload').onchange = function() {
  var img = new Image();
  img.onload = painting;
  img.src = URL.createObjectURL(this.files[0]);
  function painting() {
      ctx.drawImage(this, 0,0);
    }
}

function Undo() {
  document.getElementById("mycanvas").className = "undopng";
  window.history.go(-1);
}

function Redo() {
  document.getElementById("mycanvas").className = "redopng";
  window.history.go(1);
}




canvas.addEventListener('mousedown', function (e){
    if(eraser==true){
      isPress = true;
      old = {x: e.offsetX, y: e.offsetY};
    }
});
canvas.addEventListener('mousemove', function (e){
   if(eraser==true){
    if (isPress) {
      var x = e.offsetX;
      var y = e.offsetY;
      console.log('shit');
      ctx.globalCompositeOperation = 'destination-out';
      ctx.beginPath();
      ctx.arc(x, y, 10, 0, 2 * Math.PI);
      ctx.fill();
      ctx.lineWidth = 20;
      ctx.beginPath();
      ctx.moveTo(old.x, old.y);
      ctx.lineTo(x, y);
      ctx.stroke();
      old = {x: x, y: y};
    }
   } 
    
});
canvas.addEventListener('mouseup', function (e){
  isPress = false;
});