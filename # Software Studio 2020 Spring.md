# Software Studio 2020 Spring
## Assignment 01 Web Canvas


### Scoring

| **Basic components**                             | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Basic control tools                              | 30%       | N         |
| Text input                                       | 10%       | Y         |
| Cursor icon                                      | 10%       | Y         |
| Refresh button                                   | 10%       | Y         |

| **Advanced tools**                               | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Different brush shapes                           | 15%       | Y         |
| Un/Re-do button                                  | 10%       | Y         |
| Image tool                                       | 5%        | Y         |
| Download                                         | 5%        | Y         |

| **Other useful widgets**                         | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Name of widgets                                  | 1~5%     | N         |


---

### How to use 

    使用者進入網頁後，按下toolbar內的任意功能鍵，以及下方download和uplaod鍵，
    來開始建構一幅畫，以達到當個小畫家的體驗。

### Function description

    Brush : 在canvas中，能夠畫出不規則的線條及圖形。
    Eraser : 在canvas中，能夠消除畫布上的任何事物，但未能實作。/*當按下eraser後再去按其他功能鍵會導致其他功能鍵無法使用。*/
    Text : 在canvas中，點選後，打出一段文字且能換行，但未能backspace。
    Circle : 在canvas中，能夠畫出圓形。
    Triangle : 在canvas中，能夠畫出三角形。
    Rectangle : 在canvas中，能夠畫出長方形。
    Choose Color : 在canvas中，改變畫筆、文字的色彩。
    Brush/Font Change : 在canvas中，改變畫筆、文字的大小及字體。
    Download : 點選後，下載canvas中的圖。
    Upload : 點選後，上傳使用者電腦中的圖片至canvas中。
    
   
### Gitlab page link

    your web page URL, which should be "https://[studentID].gitlab.io/AS_01_WebCanvas"

### Others (Optional)

    抱歉助教我發現我的project無法push到gitlab上，十分抱歉[](https://)

    

<style>
table th{
    width: 100%;
}
</style>